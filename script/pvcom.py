#!/usr/bin/python3

import numpy as np
import datetime
import epics
import time
import queue
import os

## init delay to allow IOC to boot and load autosave
print("Init delay...")
time.sleep(5)

## global vars
boardqueue = queue.Queue(100)
global boardarray
boardarray = []

## create dictionary with default user names
N = 10
if "PREFIX" in os.environ:
  P = os.environ["PREFIX"]
else:
  P = "EMIL:CHAT"
print("PVcom script using prefix: {}".format(P))

pvdesc = {}
for i in range(N):
  pvdesc[f"{P}:input{i}"]=f"input{i}"



## file log
def flog(msg):
  fpath = "/EPICS/log"
  d = datetime.datetime.now()
  fname = "{}_pvcom.log".format(d.year)
  p = fpath+'/'+fname
  try:
    with open(p, 'a+') as f:
      f.write(msg+'\n')
  except Exception as err:
    print(f"[{time.asctime()}] Err: log file writing error]")
    print(err)

## format output messasge
def formatmsg(username, rawmsg):
  d = datetime.datetime.now()
  ts = "{{{:02d}.{:02d} {:02d}:{:02d}}}".format(d.month, d.day, d.hour, d.minute)
  msgstr = rawmsg.tobytes().decode("ascii").strip('\x00')
  outmsg = "{} ({}) :: {}".format(ts, username, msgstr)
  return outmsg

## handle boardarray of messages
def addtoboard(msg, N):
  global boardarray
  boardarray.append(msg)
  boardarray = boardarray[-abs(int(N)):]
  return ("\n".join(boardarray))

## function to restore previous messages loaded from autosave
def restoreboard(boardpv):
  global boardarray
  try:
    rawmsg = boardpv.get()
    rawstr = rawmsg.tobytes().decode("ascii").strip('\x00')
    boardarray = rawstr.split('\n')
  except Exception as err:
    print(f"[{time.asctime()}] Err: failed to restore previous messages]")
    print(err)


## callback for input messages
def onnewmsg(**kw):
  try:
    ## check msg length and add to queue
    nord = kw['count']
    if nord>2:
      msg = [kw['pvname'] , kw['value'].copy()]
      if boardqueue.full():
        print(f"[{time.asctime()}] Err: queue is full]")
      else:
        boardqueue.put(msg)
  except Exception as err:
    print(f"[{time.asctime()}] Err: callback error]")
    print(err)

## callback to update input DESC field as username
def updatedesc(**kw):
  try:
    pvname = kw['pvname'].split(".")[0]
    pvdesc[pvname] = kw['value']
  except Exception as err:
    print(f"[{time.asctime()}] Err: callback error]")
    print(err)

## connect PVs
inpvs = []
descpvs = []
for i in range(N):
  pvn = f"{P}:input{i}"
  inpvs.append(epics.PV(pvn, auto_monitor=True, callback=onnewmsg))
boardpv = epics.PV(f"{P}:board", auto_monitor=False)
Nentriespv = epics.PV(f"{P}:maxentries", auto_monitor=True)
for i in range(N):
  pvn = f"{P}:input{i}.DESC"
  descpvs.append(epics.PV(pvn, auto_monitor=True, callback=updatedesc))

## wait for PV connection
print("Delay for PV connections...")
time.sleep(3)

## load previous messages into array of messages
restoreboard(boardpv)

## main loop
msg = f"[{time.asctime()}] pvcom.py running..."
print(msg)
#flog(msg)
while(True):
  try:
    qobj = boardqueue.get()
    pvname = qobj[0]
    rawmsg = qobj[1]
    username = pvdesc[pvname]
    outmsg = formatmsg(username, rawmsg)
    boardpv.put(addtoboard(outmsg, Nentriespv.value))
    flog(outmsg)
    ## clean input text from source input PV
    epics.caput(pvname, " ")
  except Exception as err:
    print(f"[{time.asctime()}] Err: main loop error]")
    print(err)
    time.sleep(1)

print("OK")
