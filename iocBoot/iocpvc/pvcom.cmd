#!../../bin/linux-x86_64/pvc

## You may have to change pvc to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/pvc.dbd"
pvc_registerRecordDeviceDriver pdbbase

## test for docker
#epicsEnvSet("PREFIX", "EMIL:CHAT")

## set P as PREFIX
epicsEnvSet("P", $(PREFIX))

cd "${TOP}/iocBoot/${IOC}"

## Load record instances
dbLoadRecords("pvcom.db","P=$(P)")
dbLoadRecords("pvcom-input.db","P=$(P),R=input0")
dbLoadRecords("pvcom-input.db","P=$(P),R=input1")
dbLoadRecords("pvcom-input.db","P=$(P),R=input2")
dbLoadRecords("pvcom-input.db","P=$(P),R=input3")
dbLoadRecords("pvcom-input.db","P=$(P),R=input4")
dbLoadRecords("pvcom-input.db","P=$(P),R=input5")
dbLoadRecords("pvcom-input.db","P=$(P),R=input6")
dbLoadRecords("pvcom-input.db","P=$(P),R=input7")
dbLoadRecords("pvcom-input.db","P=$(P),R=input8")
dbLoadRecords("pvcom-input.db","P=$(P),R=input9")

## autosave
set_savefile_path("/EPICS/autosave")
set_pass1_restoreFile("auto_settings.sav")

iocInit

create_monitor_set("auto_settings.req", 30,"P=$(P)")

## Start any sequence programs
#seq sncxxx,"user=epics"
